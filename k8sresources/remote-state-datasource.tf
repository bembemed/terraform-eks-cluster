data "terraform_remote_state" "eks" {
  backend = "local"

  config = {
    path = "../AWS EKS cluster/terraform.tfstate"
  }
}