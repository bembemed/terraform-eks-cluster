#AWS instance type
variable "instance_type" {
  description = "EC2 instance type"
  type = string 
  default = "t2.micro"
}

#AWS ec2 instance key pair
variable "instance_keypair" {
  description = "Aws ec2 key pair"
  type = string
  default = "eks-terraform-key"
}

