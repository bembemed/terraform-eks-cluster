terraform {
  required_version = ">= 1.0"
  required_providers {
    aws = {
        source = "hashicorp/aws"
        version = ">= 4.6"
    }
  }
}


#provider_block
provider "aws" {
  region = var.aws_region
}