data "aws_availability_zones" "available" {
  
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"
  version = "4.0.1"

  #vpc basiv details
  name = "${local.name}-${var.vpc_name}"
  cidr = var.vpc_cidr_block
  azs = data.aws_availability_zones.available.names
  public_subnets = var.vpc_public_subnets
  private_subnets = var.vpc_private_subnets

  
  #vpc Database
  database_subnets = var.vpc_database_subnets
  create_database_subnet_group = var.vpc_create_database_subnet_group
  create_database_subnet_route_table = var.vpc_create_database_subnet_route_table

  #create_database_internet_gateway_route = true
  #create_database_nat_gateway_route = true

  #Nat gateways - Outbount communication
  enable_nat_gateway = var.vpc_enable_nat_gateway
  single_nat_gateway = var.vpc_single_nat_gateway

  #vpc dns parametres
  enable_dns_hostnames = true
  enable_dns_support = true

  tags = local.common_tags
  vpc_tags = local.common_tags

  #additionnal tags to subnet
  public_subnet_tags = {
    Type = "public subnets"
  }

  private_subnet_tags = {
    Type = "private subnets"
  }

  database_subnet_tags = {
    Type = "private database subnets"
  }

  # Instances launched into the Public subnet should be assigned a public IP address.
  map_public_ip_on_launch = true

}