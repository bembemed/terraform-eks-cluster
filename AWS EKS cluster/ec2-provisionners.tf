resource "null_resource" "copy_ec2_keys" {
  depends_on = [ module.ec2_public ]

  #Connection block
  connection {
    type = "ssh"
    host = aws_eip.bastion_eip.public_ip
    user = "ec2-user"
    password = ""
    private_key = file("./private key/eks-terraform-key.pem")
  }

  ##File provisionner
  provisioner "file" {
    source = "./private key/eks-terraform-key.pem"
    destination = "/tmp/eks-terraform-key.pem"
  }

  ##remote exec provisionner
  provisioner "remote-exec" {
    inline = [ 
        "sudo chmod 777 /tmp/eks-terraform-key.pem"
     ]
  }

  ## local exe
  provisioner "local-exec" {
    command = "echo VPC created on `date`  and vpc id: ${module.vpc.vpc_id} >> creation-time-vpc-id.txt"
    working_dir = "local-exec-output-files/"
  }
}