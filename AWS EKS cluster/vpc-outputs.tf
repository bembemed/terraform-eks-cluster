#vpc_id
output "vpc_id" {
  description = "the ID of VPC"
  value = module.vpc.vpc_id
}

#VPC cidr_block
output "vpc_cidr_blocks" {
  description = "the CIDR blocks of the VPC"
  value = module.vpc.vpc_cidr_block
}

#vpc_ private subnets
output "vpc_private_subnets" {
  description = "List of IDs of private subnets"
  value = module.vpc.private_subnets
}

#vpc_ public subnets
output "vpc_pubic_subnets" {
  description = "List of IDs of public subnets"
  value = module.vpc.public_subnets
}

#vpc_nat_gateways_public ip
output "nat_public_ips" {
  description = "List of public Elastic IPs created for AWS NAT Gateways"
  value = module.vpc.nat_public_ips
}

#VPC AZs
output "azs" {
  description = "A list of availability zones specified  as argument  to this module"
  value = module.vpc.azs
}